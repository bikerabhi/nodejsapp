const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World! This is node js app')
})

app.get('/versions', (req, res) => {
    res.json([{
      name:"test",
      version:"1.0"
    },
    {
      name:"test-2",
      version:"2.0"
    },
    {
      name:"test-3",
      version:"3.0"
    }
  ])
  })

app.listen(port, () => {
  console.log(`Node.js app listening on port ${port}`)
})